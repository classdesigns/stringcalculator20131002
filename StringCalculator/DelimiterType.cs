﻿namespace StringCalculator
{
    public enum DelimiterType
    {
        Unspecified = 0
        , Default // \n and , e.g. a regex of [,\n]
        , SingleCharacter // single char between // and \n
        , SingleSequence // single sequence encapsulated by [ and ] e.g. [***]
        , MultiSequence // multiple sequences encapsulated by multiple [ and ] e.g. [*][%]
        
    }
}
