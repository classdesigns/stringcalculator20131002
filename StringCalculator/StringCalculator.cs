using System.Collections.Generic;
using StringCalculator.Interfaces;

namespace StringCalculator
{
    public class StringCalculator
    {
        private readonly IDelimiterAndNumberDeterminator _delimiterAndNumberDeterminator;

        public StringCalculator(IDelimiterAndNumberDeterminator delimiterAndNumberDeterminator)
        {
            _delimiterAndNumberDeterminator = delimiterAndNumberDeterminator;
        }

        public int Add(string numbersToAdd)
        {
            int returnValue = 0;
            if (string.IsNullOrEmpty(numbersToAdd))
                return returnValue;

            foreach (int number in GetNumbersToAdd(numbersToAdd))
            {
                returnValue += (number);
            }

            return returnValue;
        }

        private IEnumerable<int> GetNumbersToAdd(string numbersToAdd)
        {
            List<int> numbersToAddAsInts = new List<int>();

            foreach (var number in GetNumbersLessThan1001AsIntsThrowExceptionIfAnyAreNegative(numbersToAdd))
            {
                numbersToAddAsInts.Add(number);
            }

            return numbersToAddAsInts;
        }

        private List<int>  GetNumbersLessThan1001AsIntsThrowExceptionIfAnyAreNegative(string numbers)
        {
            List<int> numbersAsInts = new List<int>();

            foreach (var number in GetStringNumbersToAdd(numbers))
            {
                int numberToCheckIsLessThanOrEqualTo1000 = int.Parse(number);
                if (numberToCheckIsLessThanOrEqualTo1000 <= 1000)
                    numbersAsInts.Add(numberToCheckIsLessThanOrEqualTo1000);
            }

            bool containsNegativeNumber = false;
            foreach (var negativeNumberCandidate in numbersAsInts)
            {
                if(negativeNumberCandidate < 0)
                    containsNegativeNumber = true;
            }

            if (containsNegativeNumber)
                throw new NegativeNumbersNotAllowedException(numbersAsInts);
            
            return numbersAsInts;
        }

        private IEnumerable<string> GetStringNumbersToAdd(string calculatorInputString)
        {
            return _delimiterAndNumberDeterminator.GetStringNumbers(calculatorInputString);
        }
    }
}