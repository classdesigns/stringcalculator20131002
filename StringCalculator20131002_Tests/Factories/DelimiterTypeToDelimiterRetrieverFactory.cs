﻿using System.Collections.Generic;
using StringCalculator;
using StringCalculator.DelimiterRetrievers;

namespace StringCalculator20131002_Tests.Factories
{
    public class DelimiterTypeToDelimiterRetrieverFactory
    {
        public static Dictionary<DelimiterType, IDelimiterRetriever> GetDelimiterTypeToRetrieverMap()
        {
            Dictionary<DelimiterType, IDelimiterRetriever> delimiterRetrieverMap =
                new Dictionary<DelimiterType, IDelimiterRetriever>();

            delimiterRetrieverMap.Add(DelimiterType.Default, new DefaultDelimiterRetriever());
            var regexStringEscaper = new RegexStringEscaper();
            delimiterRetrieverMap.Add(DelimiterType.SingleCharacter,
                                      new SingleCharacterDelimiterRetriever(regexStringEscaper));
            delimiterRetrieverMap.Add(DelimiterType.SingleSequence, new SingleSequenceDelimiterRetriever(regexStringEscaper));
            delimiterRetrieverMap.Add(DelimiterType.MultiSequence, new MultiSequenceDelimiterRetriever(regexStringEscaper));

            return delimiterRetrieverMap;
        }
    }
}