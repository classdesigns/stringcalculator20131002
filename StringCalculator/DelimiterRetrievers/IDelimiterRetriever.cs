namespace StringCalculator.DelimiterRetrievers
{
    public interface IDelimiterRetriever
    {
        string GetRegexPreparedDelimiter(string stringCalculatorInput);
    }
}