﻿using NUnit.Framework;
using StringCalculator;
using StringCalculator.DelimiterRetrievers;

namespace SingleCharacterDelimiterRetriever20131001_Tests
{
    [TestFixture]
    public class TheSingleCharacterDelimiterRetriever
    {
        [TestFixture]
        public class GetRegexPreparedDelimiter
        {
            private IDelimiterRetriever _singleCharacterDelimiterRetriever;

            [SetUp]
            public void Setup()
            {
                _singleCharacterDelimiterRetriever = new SingleCharacterDelimiterRetriever(new RegexStringEscaper());
            }

            [Test]
            public void ReturnsSemiColon_WhenInputIsSlashSlashSemicolonBackslashN1Semicolon2()
            {
                const string expectedDelimiterOfSemicolon = ";";
                const string slashSlashSemicolonBackslashN1Semicolon2 = "//;\n1;2";

                var returnedDelimiter =
                    _singleCharacterDelimiterRetriever.GetRegexPreparedDelimiter(
                        slashSlashSemicolonBackslashN1Semicolon2);

                Assert.That(returnedDelimiter, Is.EqualTo(expectedDelimiterOfSemicolon));
            }

            [Test]
            public void ReturnsBackslashAsterisk_WhenInputIsSlashSlashAsteriskBackslashN1Asterisk2()
            {
                const string expectedBackslashAsterisk = "\\*";
                const string slashSlashAsteriskBackslashN1Semicolon2 = "//*\n1*2";

                var returnedDelimiter =
                    _singleCharacterDelimiterRetriever.GetRegexPreparedDelimiter(slashSlashAsteriskBackslashN1Semicolon2);

                Assert.That(returnedDelimiter, Is.EqualTo(expectedBackslashAsterisk));
            }
        }
    }
}
