﻿using System;
using StringCalculator.Interfaces;

namespace StringCalculator
{
    public class DelimiterTypeDeterminator : IDelimiterTypeDeterminator
    {
        private const string SlashN = "\n";

        public DelimiterType Determine(string stringCalculatorInput)
        {
            if(DoesNotStartsWithSlashSlash(stringCalculatorInput) || stringCalculatorInput.IndexOf(SlashN, StringComparison.Ordinal) <= 0)
                return DelimiterType.Default;

            if(IsSingleCharacterDelimiter(stringCalculatorInput))
                return DelimiterType.SingleCharacter;

            if(IsSingleSequenceDelimiter(stringCalculatorInput))
                return DelimiterType.SingleSequence;

            if(IsMultiSequenceDelimiter(stringCalculatorInput))
                return DelimiterType.MultiSequence;
            
            
            return DelimiterType.Unspecified;
        }

        private bool IsMultiSequenceDelimiter(string stringCalculatorInput)
        {
            return stringCalculatorInput.GetCountOfOpenBrackets() > 1 &&
                   stringCalculatorInput.GetCountOfClosedSquareBrackets() > 1 &&
                   AreOpenAndClosedBracketCountsEqual(stringCalculatorInput);  
        }

        private bool AreOpenAndClosedBracketCountsEqual(string stringCalculatorInput)
        {
            return stringCalculatorInput.GetCountOfOpenBrackets()
                                     .Equals(stringCalculatorInput.GetCountOfClosedSquareBrackets());
        }

        private bool IsSingleSequenceDelimiter(string stringCalculatorInput)
        {
            bool containsOnly1OpenSquareBracket = stringCalculatorInput.GetCountOfOpenBrackets().Equals(1);
            bool containsOnly1ClosingSquareBracket = stringCalculatorInput.GetCountOfClosedSquareBrackets().Equals(1);

            return containsOnly1OpenSquareBracket && containsOnly1ClosingSquareBracket;
        }

        public bool NoDelimiterIsSpecified(string stringCalculatorInput)
        {
            return DoesNotStartsWithSlashSlash(stringCalculatorInput) || stringCalculatorInput.IndexOf(SlashN, StringComparison.Ordinal) <= 0;
        }

        private bool IsSingleCharacterDelimiter(string stringCalculatorInput)
        {
            int firstDelimiter = stringCalculatorInput.IndexOf("//", StringComparison.Ordinal) + 2;
            int secondDelimiter = stringCalculatorInput.IndexOf('\n');
            bool lengthBetweenStartAndStopDelimiterCharsIs1 = secondDelimiter - firstDelimiter == 1;

            return lengthBetweenStartAndStopDelimiterCharsIs1;
        }

        private static bool DoesNotStartsWithSlashSlash(string stringCalculatorInput)
        {
            return !stringCalculatorInput.StartsWith("//");
        }
    }
}