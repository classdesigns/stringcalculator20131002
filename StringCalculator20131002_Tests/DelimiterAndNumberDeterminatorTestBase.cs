﻿using NUnit.Framework;
using StringCalculator;
using StringCalculator.Interfaces;
using StringCalculator20131002_Tests;
using StringCalculator20131002_Tests.Factories;

namespace DelimiterDeterminator20131002_Tests
{
    public class DelimiterAndNumberDeterminatorTestBase
    {
        protected IDelimiterAndNumberDeterminator DelimiterNumberDeterminator;
        [SetUp]
        public void Setup()
        {
            DelimiterNumberDeterminator = new DelimiterAndNumberDeterminator(DelimiterTypeToDelimiterRetrieverFactory.GetDelimiterTypeToRetrieverMap(),new DelimiterTypeDeterminator());
        }
    }
}