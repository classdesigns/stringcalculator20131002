using System.Collections.Generic;
using System.Globalization;
using System.Text;
using StringCalculator.Interfaces;

namespace StringCalculator
{
    public class RegexStringEscaper : IRegexStringEscaper
    {
        private readonly List<string> _regexCharactersRequiringEscaping = GetRegexEscapeCharacters();

        public string GetStringEscapedForUseAsRegex(string unEscapedInputString)
        {
            StringBuilder escapedChars = new StringBuilder();

            foreach (var character in unEscapedInputString)
            {
                var singleEscapedChar = GetSingleEscapedChar(character.ToString(CultureInfo.InvariantCulture));
                escapedChars.Append(singleEscapedChar);
            }

            return escapedChars.ToString();
        }

        private string GetSingleEscapedChar(string character)
        {
            if (!_regexCharactersRequiringEscaping.Contains(character))
                return character;

            return "\\" + character;
        }

        private static List<string> GetRegexEscapeCharacters()
        {
            List<string> escapeChars = new List<string>();
            escapeChars.Add("\\");
            escapeChars.Add("^");
            escapeChars.Add("$");
            escapeChars.Add(".");
            escapeChars.Add("|");
            escapeChars.Add("?");
            escapeChars.Add("*");
            escapeChars.Add("+");
            escapeChars.Add("(");
            escapeChars.Add(")");
            escapeChars.Add("[");
            escapeChars.Add("{");

            return escapeChars;
        }
    }
}