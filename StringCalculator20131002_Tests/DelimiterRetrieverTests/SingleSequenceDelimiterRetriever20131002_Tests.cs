﻿using NUnit.Framework;
using StringCalculator;
using StringCalculator.DelimiterRetrievers;

namespace SingleSequenceDelimiterRetriever20131002_Tests
{
    [TestFixture]
    public class GetRegexPreparedDelimiter
    {
        [Test]
        public void
            ReturnsBackslashAsteriskBackslashAsteriskBackslashAsterisk_WhenInputIsSlashSlashBracketAsteriskAsteriskAsteriskBracketBackslashN1AsteriskAsteriskAsterisk2
            ()
        {
            const string slashSlashBracketAsteriskAsteriskAsteriskBracketBackslashN1AsteriskAsteriskAsterisk2 =
                "//[***]\n1***2";
            const string expectedBackslashAsteriskBackslashAsteriskBackslashAsterisk = "\\*\\*\\*";
            IDelimiterRetriever singleSequenceDelimiter = new SingleSequenceDelimiterRetriever(new RegexStringEscaper());

            var returnedDelimiter =
                singleSequenceDelimiter.GetRegexPreparedDelimiter(
                    slashSlashBracketAsteriskAsteriskAsteriskBracketBackslashN1AsteriskAsteriskAsterisk2);

            Assert.That(returnedDelimiter, Is.EqualTo(expectedBackslashAsteriskBackslashAsteriskBackslashAsterisk));
        }
    }
}