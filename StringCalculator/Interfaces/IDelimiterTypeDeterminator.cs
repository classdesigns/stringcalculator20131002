﻿namespace StringCalculator.Interfaces
{
    public interface IDelimiterTypeDeterminator
    {
        DelimiterType Determine(string stringCalculatorInput);
    }
}