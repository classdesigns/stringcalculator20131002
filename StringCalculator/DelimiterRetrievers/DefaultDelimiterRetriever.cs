﻿namespace StringCalculator.DelimiterRetrievers
{
    public class DefaultDelimiterRetriever : IDelimiterRetriever
    {
        public string GetRegexPreparedDelimiter(string stringCalculatorInput)
        {
            return "[,\n]";
        }
    }
}