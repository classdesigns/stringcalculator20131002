﻿using System.Linq;

namespace StringCalculator
{
    public static class StringExtensions
    {
        private static int GetCountOfCharacter(string stringToCheck, char characterToSearchFor)
        {
            return stringToCheck.Count(c => c.Equals(characterToSearchFor));
        }

        public static int GetCountOfClosedSquareBrackets(this string stringCalculatorInput)
        {
            return GetCountOfCharacter(stringCalculatorInput, ']');
        }

        public static int GetCountOfOpenBrackets(this string stringCalculatorInput)
        {
            return GetCountOfCharacter(stringCalculatorInput, '[');
        }
    }
}