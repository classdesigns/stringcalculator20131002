﻿using System.Collections.Generic;
using System.Linq;
using DelimiterDeterminator20131002_Tests;
using NUnit.Framework;
using StringCalculator;
using StringCalculator.Interfaces;
using StringCalculator20131002_Tests.Factories;

namespace StringCalculator20131002_Tests.DelimiterAndNumberDeterminator_Tests
{
    [TestFixture]
    public class TheDelimiterAndNumberDeterminator
    {
        [Test]
        public void Implements_IDelimiterAndNumberDeterminator()
        {
            DelimiterAndNumberDeterminator delimiterAndNumber = new DelimiterAndNumberDeterminator(DelimiterTypeToDelimiterRetrieverFactory.GetDelimiterTypeToRetrieverMap(),new DelimiterTypeDeterminator());

            var delimiterAsIDelimiter = delimiterAndNumber as IDelimiterAndNumberDeterminator;
            
            Assert.That(delimiterAsIDelimiter, Is.InstanceOf<IDelimiterAndNumberDeterminator>());
        }
    }

    [TestFixture]
    public class GetDelimiter : DelimiterAndNumberDeterminatorTestBase
    {
       [Test]
        public void ReturnsCharactersBetween_ForwardSlashForwardSlash_And_BackSlashN()
        {
            const string expectedDelimimterOfSemiColon = ";";
            const string forwardSlashForwardSlashSemicolonBackslashN = "//;\n";
            
            var delimiterRetrieved = DelimiterNumberDeterminator.GetDelimiter(forwardSlashForwardSlashSemicolonBackslashN);
           
            Assert.That(delimiterRetrieved, Is.EqualTo(expectedDelimimterOfSemiColon));
        }

        [Test]
        public void ReturnsCommaBackslashN_WhenNoDelimiterChangerCharactersForwardSlashForwardSlashAreFound()
        {
            const string expectedDelimterOfOpenBracketCommaBackslashNCloseBracket = "[,\n]";
            const string oneComma2 = "1,2";
            
            var actualDelimiter = DelimiterNumberDeterminator.GetDelimiter(oneComma2);

            Assert.That(actualDelimiter, Is.EqualTo(expectedDelimterOfOpenBracketCommaBackslashNCloseBracket));
        }

        [Test]
        public void ReturnsRegexReadyEscapedAsteriskAsteriskAsterisk_WhenStringArgumentPassedContainsMultiCharacterDelimiterOfSquareBracketAsteriskAsteriskAsteriskAndNumbers1And2And3()
        {
            const string expectedDelimiterAsteriskAsteriskAsterisk = "\\*\\*\\*";
            
            var delimiter = DelimiterNumberDeterminator.GetDelimiter("//[***]\n1***2***3");

            Assert.That(delimiter, Is.EqualTo(expectedDelimiterAsteriskAsteriskAsterisk));
        }
    }

    [TestFixture]
    public class NoDelimiterIsSpecified : DelimiterAndNumberDeterminatorTestBase
    {
       
        [Test]
        public void ReturnsTrue_WhenStringArgumentDoesNotStartWithForwardSlashForwardSlash()
        {
            const bool expectedValueOfTrue = true;
            
            var actualValueReturned = DelimiterNumberDeterminator.NoDelimiterIsSpecified("1,2");
           
            Assert.That(actualValueReturned, Is.EqualTo(expectedValueOfTrue));
        }

        [Test]
        public void ReturnsFalse_WhenStringArgumentStartsWithForwardSlashForwardSlashAndContainsBackslashN()
        {
            const bool expectedValueOfFalse = false;
            
            var actualValueReturned = DelimiterNumberDeterminator.NoDelimiterIsSpecified("//\n");
            
            Assert.That(actualValueReturned, Is.EqualTo(expectedValueOfFalse));
        }
    }

    [TestFixture]
    public class GetStringNumbers : DelimiterAndNumberDeterminatorTestBase
    {
        [Test]
        public void Returns1And2And3_WhenStringArgumentPassedIsForwardSlashForwardSlashSemicolonBackslashN3Semicolon2Semicolon3()
        {
            const string forwardSlashForwardSlashSemicolonBackslashN3Semicolon2Semicolon3 = "//;\n3;2;3";
            IEnumerable<string> expectedIEnumerable = new[] {"3", "2", "3"};
            IEnumerable<string> numbersToTest = DelimiterNumberDeterminator.GetStringNumbers(forwardSlashForwardSlashSemicolonBackslashN3Semicolon2Semicolon3);

            for (int i = 0; i < 3; i++)
            {
                Assert.That(numbersToTest.ElementAt(i), Is.EqualTo(expectedIEnumerable.ElementAt(i)));
            }          
        }

        [Test]
        public void Returns1And2And3_WhenStringArgumentPassedContainsMultiCharacterDelimiterOfAsteriskAsteriskAsteriskAndNumbers1And2And3()
        {
            List<string> expectedNumbers = new List<string> { "1", "2", "3" };
            var numbersToTest = DelimiterNumberDeterminator.GetStringNumbers("//[***]\n1***2***3");
            for (int i = 0; i < 3; i++)
            {
                Assert.That(numbersToTest.ElementAt(i), Is.EqualTo(expectedNumbers.ElementAt(i)));
            }
        }
    }
}
