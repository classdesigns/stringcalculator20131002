using System;
using System.Collections.Generic;

namespace StringCalculator
{
    public class NegativeNumbersNotAllowedException : ArgumentOutOfRangeException
    {
        private string _generatedErrorMessage;

        public NegativeNumbersNotAllowedException(IEnumerable<int> numbers)
        {
            GenerateMessageFromNumbers(numbers);

        }

        private void GenerateMessageFromNumbers(IEnumerable<int> numbers)
        {
            List<int> negativeNumbers = GetNegativeNumbers(numbers);

            _generatedErrorMessage = "Negatives not allowed: " + string.Join(",", negativeNumbers);
        }

        private List<int> GetNegativeNumbers(IEnumerable<int> numbers)
        {
            List<int> negativeNumbers = new List<int>();

             foreach (var negativeNumberCandidate in numbers)
            {
                if(negativeNumberCandidate < 0)
                    negativeNumbers.Add(negativeNumberCandidate);
            }

            return negativeNumbers;
        }

        public override string Message
        {
            get { return _generatedErrorMessage; }
        }
    }
}