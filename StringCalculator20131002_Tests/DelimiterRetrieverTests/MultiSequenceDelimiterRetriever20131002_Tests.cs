﻿using NUnit.Framework;
using StringCalculator;
using StringCalculator.DelimiterRetrievers;
using StringCalculator.Interfaces;

namespace MultiSequenceDelimiterRetriever20131002_Tests
{
    [TestFixture]
    public class GetRegexPreparedDelimiter
    {
        private IDelimiterRetriever _multiSequenceDelimiterRetriever;

        [SetUp]
        public void Setup()
        {
            IRegexStringEscaper regexStringEscaper = new RegexStringEscaper();
            _multiSequenceDelimiterRetriever = new MultiSequenceDelimiterRetriever(regexStringEscaper);
        }
        
        [Test]
        public void ReturnsOpenSquareBracketQWClosedSquareBracket_WhenForwardSlashForwardSlashOpenSquareBracketQCloseSquareBracketOpenSquareBracketWCloseSquareBracketBackslashN1Q2W3_PassedIn()
        {
            const string expectedValueOfOpenSquareBracketQWClosedSquareBracket = "[QW]";
            const string forwardSlashForwardSlashOpenSquareBracketQCloseSquareBracketOpenSquareBracketWCloseSquareBracketBackslashN1Q2W3 = "//[Q][W]\n1Q2W3";
            
            var returnedDelimiter = _multiSequenceDelimiterRetriever.GetRegexPreparedDelimiter(forwardSlashForwardSlashOpenSquareBracketQCloseSquareBracketOpenSquareBracketWCloseSquareBracketBackslashN1Q2W3);
            
            Assert.That(returnedDelimiter, Is.EqualTo(expectedValueOfOpenSquareBracketQWClosedSquareBracket));
        }

        [Test]
        public void ReturnsOpenSquareBracketBackslashAsteriskPercentClosedSquareBracket_WhenForwardSlashForwardSlashOpenSquareBracketAsteriskCloseSquareBracketOpenSquareBracketPercentCloseSquareBracketBackslashN1Asterisk2Percent3()
        {
            const string expectedValueOfOpenSquareBracketBackslashAsteriskPercentClosedSquareBracket = "[\\*%]";
            const string forwardSlashForwardSlashOpenSquareBracketAsteriskCloseSquareBracketOpenSquareBracketPercentCloseSquareBracketBackslashN1Asterisk2Percent3 = "//[*][%]\n1*2%3";
            
            var actualDelimiterReturned =
                _multiSequenceDelimiterRetriever.GetRegexPreparedDelimiter(
                    forwardSlashForwardSlashOpenSquareBracketAsteriskCloseSquareBracketOpenSquareBracketPercentCloseSquareBracketBackslashN1Asterisk2Percent3);

            Assert.That(actualDelimiterReturned, Is.EqualTo(expectedValueOfOpenSquareBracketBackslashAsteriskPercentClosedSquareBracket));
        }
    }
}
