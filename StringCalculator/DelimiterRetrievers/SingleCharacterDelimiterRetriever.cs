﻿using System;
using StringCalculator.Interfaces;

namespace StringCalculator.DelimiterRetrievers
{
    public class SingleCharacterDelimiterRetriever : IDelimiterRetriever
    {
        private readonly IRegexStringEscaper _regexStringEscaper;

        public SingleCharacterDelimiterRetriever(IRegexStringEscaper regexStringEscaper)
        {
            _regexStringEscaper = regexStringEscaper;
        }

        public string GetRegexPreparedDelimiter(string stringCalculatorInput)
        {
            int firstDelimiter = stringCalculatorInput.IndexOf("//", StringComparison.Ordinal) + 2;
            int secondDelimiter = stringCalculatorInput.IndexOf('\n');

            string nonRegexPreparedDelimiter = stringCalculatorInput.Substring(firstDelimiter, secondDelimiter - firstDelimiter);
            
            return _regexStringEscaper.GetStringEscapedForUseAsRegex(nonRegexPreparedDelimiter);
        }
    }
}