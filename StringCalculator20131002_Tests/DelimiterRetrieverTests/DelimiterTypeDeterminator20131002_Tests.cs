﻿using NUnit.Framework;
using StringCalculator;
using StringCalculator.Interfaces;

namespace DelimiterTypeDeterminator20131002_Tests
{
    [TestFixture]
    public class TheDeterminator
    {
        [Test]
        public void Implements_IDelimiterTypeDeterminator()
        {
            DelimiterTypeDeterminator determinator = new DelimiterTypeDeterminator();
            
            var delimiterAsIDelimiterTypeDeterminator = determinator as IDelimiterTypeDeterminator;
            
            Assert.That(delimiterAsIDelimiterTypeDeterminator, Is.Not.Null);
        }
    }

    [TestFixture]
    public class Determine
    {
        private IDelimiterTypeDeterminator _determinator;

        [SetUp]
        public void Setup()
        {
            _determinator = new DelimiterTypeDeterminator();
        }

        [Test]
        public void ReturnsDefault_WhenInputStringDoesNotContainForwardSlashForwardSlash()
        {
            const DelimiterType expectedDelimiterTypeOfDefault = DelimiterType.Default;
           
            var actualDelimiterType = _determinator.Determine("1,2,3");

            Assert.That(actualDelimiterType, Is.EqualTo(expectedDelimiterTypeOfDefault));
            
        }

        [Test]
        public void ReturnsSingleCharacter_WhenInputStringStartswithForwardSlashForwardSlashSemicolonBackslashN()
        {
            DelimiterType expectedDelimiterTypeOfSingleCharacter = DelimiterType.SingleCharacter;

            var actualDelimiterType = _determinator.Determine("//;\n");

            Assert.That(actualDelimiterType, Is.EqualTo(expectedDelimiterTypeOfSingleCharacter));
        }

        [Test]
        public void ReturnsSingleSequence_WhenInputStringStartsWithForwardSlashForwardSlashOpenSquareBracketAsteriskAsteriskAsteriskCloseSquareBracketBackslashN()
        {
            DelimiterType expectedDelimiterTypeOfSingleSequence = DelimiterType.SingleSequence;
            const string forwardSlashForwardSlashOpenSquareBracketAsteriskAsteriskAsteriskCloseSquareBracketBackslashN = "//[***]\n";
            
            var actualDelimiterType =
                _determinator.Determine(forwardSlashForwardSlashOpenSquareBracketAsteriskAsteriskAsteriskCloseSquareBracketBackslashN);

            Assert.That(actualDelimiterType, Is.EqualTo(expectedDelimiterTypeOfSingleSequence));
        }

        [Test]
        public void ReturnsMultiSequence_WhenInputStringStartsWithSlashSlashOpenBracketAsteriskCloseBracketOpenBracketPercentCloseBracketBackslashN()
        {
            DelimiterType expectedDelimiterTypeOfMultiSequence = DelimiterType.MultiSequence;
            const string slashSlashOpenBracketAsteriskCloseBracketOpenBracketPercentCloseBracketBackslashN = "//[*][%]\n";
            
            var actualDelimiterType = _determinator.Determine(slashSlashOpenBracketAsteriskCloseBracketOpenBracketPercentCloseBracketBackslashN);
            
            Assert.That(actualDelimiterType, Is.EqualTo(expectedDelimiterTypeOfMultiSequence));
        }
    }
}
