﻿using NUnit.Framework;
using StringCalculator;
using StringCalculator.Interfaces;
using StringCalculator20131002_Tests.Factories;

namespace StringCalculator20131002_Tests
{
    [TestFixture]
    public class Constructor
    {
        
        [Test]
        public void TakesIDelimiterDeterminator()
        {
            IDelimiterAndNumberDeterminator delimiterAndNumberDeterminator = new DelimiterAndNumberDeterminator(DelimiterTypeToDelimiterRetrieverFactory.GetDelimiterTypeToRetrieverMap(), new DelimiterTypeDeterminator());
            StringCalculator.StringCalculator calculator = new StringCalculator.StringCalculator(delimiterAndNumberDeterminator);
            Assert.That(calculator, Is.Not.Null);
        }
    }


    [TestFixture]
    public class Add
    {
        private StringCalculator.StringCalculator _calculator;

        [SetUp]
        public void Setup()
        {
            _calculator = new StringCalculator.StringCalculator(new DelimiterAndNumberDeterminator(DelimiterTypeToDelimiterRetrieverFactory.GetDelimiterTypeToRetrieverMap(), new DelimiterTypeDeterminator()));
        }

        [Test]
        public void TakesString_AsArgument()
        {
            Assert.That(_calculator.Add(string.Empty), Is.Not.Null);
        }

        [Test]
        public void Returns_TypeOfInt()
        {
            var actualReturnValue = _calculator.Add(string.Empty);
            
            Assert.That(actualReturnValue, Is.TypeOf<int>());
        }

        [Test]
        public void Returns0_WhenEmptyStringIsPassed()
        {
            const int expectedValueOf0 = 0;
            
            var actualValue = _calculator.Add(string.Empty);

            Assert.That(actualValue, Is.EqualTo(expectedValueOf0));
        }

        [Test]
        public void Returns1_WhenStringCharacter1IsPassed()
        {
            const int expectedValueOf1 = 1;

            var actualValue = _calculator.Add("1");

            Assert.That(actualValue, Is.EqualTo(expectedValueOf1));
        }

        [Test]
        public void Returns2_WhenStringCharacters1Comma1ArePassed()
        {
            const int expectedValueOf2 =2;
            const string stringCharacters1Comma1 = "1,1";
           
            var actualValue = _calculator.Add(stringCharacters1Comma1);
            
            Assert.That(actualValue, Is.EqualTo(expectedValueOf2));
        }

        [Test]
        public void Returns9_WhenStringCharacters4Comma3Comma2ArePassed()
        {
            const int expectedValueOf9 = 9;
            const string stringCharacters4Comma3Comma2 = "4,3,2";
            
            var actualValue = _calculator.Add(stringCharacters4Comma3Comma2);
            
            Assert.That(actualValue, Is.EqualTo(expectedValueOf9));
        }

        [Test]
        public void Returns6_WhenUsingSlashAsWellAsCommaAsNumberDelimiters_1SlashN2Comma3()
        {
            const int expectedValueOf6 = 6;
            const string oneSlashN2Comma3 = "1\n2,3";
            
            var actualValue = _calculator.Add(oneSlashN2Comma3);
            
            Assert.That(actualValue, Is.EqualTo(expectedValueOf6));
        }

        [Test]
        public void Returns3_WhenUsingSemicolonAsTheDelimiter()
        {
            const int expectedValueOf3 = 3;
            const string forwardSlashForwardSlashSemiColonBackslashN1SemiColon2 = "//;\n1;2";
            
            var actualReturnValue = _calculator.Add(forwardSlashForwardSlashSemiColonBackslashN1SemiColon2);
            
            Assert.That(actualReturnValue, Is.EqualTo(expectedValueOf3));
        }

        [Test]
        public void Returns8_WhenPassedForwardSlashForwardSlahSemicolonBackslashN3Semicolon2Semicolon3()
        {
            const string forwardSlashForwardSlashSemicolonBackslashN3Semicolon2Semicolon3 = "//;\n3;2;3";
            const int expectedValueOf8 = 8;
            
            var actualValueReturned = _calculator.Add(forwardSlashForwardSlashSemicolonBackslashN3Semicolon2Semicolon3);

            Assert.That(actualValueReturned, Is.EqualTo(expectedValueOf8));
        }

        [Test]
        [ExpectedException(typeof(NegativeNumbersNotAllowedException))]
        public void ThrowsNegativeNumbersNotAllowedException_WhenFirstNumberIsNegative()
        {
            const string negative1Comma2Comma3 = "-1,2,3";
            _calculator.Add(negative1Comma2Comma3);
        }

        [Test]
        [ExpectedException(typeof(NegativeNumbersNotAllowedException), ExpectedMessage = "Negatives not allowed: -3,-5")]
        public void NegativeNumbersNotAllowedExceptionMessage_ContainsNegative3AndNegative5_WhenInputStringIs1Comma2CommaNegative3Comma4CommaNegative5()
        {
            const string oneComma2CommaNegative3Comma4CommaNegative5 = "1,2,-3,4,-5";
            _calculator.Add(oneComma2CommaNegative3Comma4CommaNegative5);
        }

        [Test]
        public void NumbersGreaterThan1000ShouldBeIgnored_SoInputStringOf4Comma5Comma1001_MustEqual9()
        {
            const int expectedValueOf9 = 9;
            const string fourComma5Comma1001 = "4,5,1001";
            
            var actualValue = _calculator.Add(fourComma5Comma1001);
            
            Assert.That(actualValue, Is.EqualTo(expectedValueOf9));
        }

        [Test]
        public void Returns6_WhenForwardSlashForwardSlashOpenSquareBracketAsteriskCloseSquareBracketOpenSquareBracketPercentCloseSquareBracketBackslashN1Asterisk2Percent3()
        {
            const int expectedValueOf6 = 6;
            const string forwardSlashForwardSlashOpenSquareBracketAsteriskCloseSquareBracketOpenSquareBracketPercentCloseSquareBracketBackslashN1Asterisk2Percent3 = "//[*][%]\n1*2%3";

            var actualDelimiterReturned = _calculator.Add(forwardSlashForwardSlashOpenSquareBracketAsteriskCloseSquareBracketOpenSquareBracketPercentCloseSquareBracketBackslashN1Asterisk2Percent3);

            Assert.That(actualDelimiterReturned, Is.EqualTo(expectedValueOf6));
        }
    }
}
