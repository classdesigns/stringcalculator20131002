namespace StringCalculator.Interfaces
{
    public interface IRegexStringEscaper
    {
        string GetStringEscapedForUseAsRegex(string unEscapedInputString);
    }
}