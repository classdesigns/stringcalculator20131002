using StringCalculator.Interfaces;

namespace StringCalculator.DelimiterRetrievers
{
    public class SingleSequenceDelimiterRetriever : IDelimiterRetriever
    {
        private readonly IRegexStringEscaper _regexStringEscaper;

        public SingleSequenceDelimiterRetriever(IRegexStringEscaper regexStringEscaper)
        {
            _regexStringEscaper = regexStringEscaper;
        }

        public string GetRegexPreparedDelimiter(string stringCalculatorInput)
        {
            int delimiterStart = stringCalculatorInput.IndexOf('[') + 1;
            int delimiterStop = stringCalculatorInput.IndexOf(']');
            int lengthOfDelimiter = delimiterStop - delimiterStart;

            return
                _regexStringEscaper.GetStringEscapedForUseAsRegex(stringCalculatorInput.Substring(delimiterStart,
                                                                                                  lengthOfDelimiter));
        }
    }
}