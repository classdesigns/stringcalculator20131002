using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using StringCalculator.DelimiterRetrievers;
using StringCalculator.Interfaces;

namespace StringCalculator
{
    public class DelimiterAndNumberDeterminator : IDelimiterAndNumberDeterminator
    {
        private const string SlashN = "\n";
        private readonly IDelimiterTypeDeterminator _delimiterTypeDeterminator;
        private readonly Dictionary<DelimiterType, IDelimiterRetriever> _delimiterRetrievers;


        public DelimiterAndNumberDeterminator(Dictionary<DelimiterType, IDelimiterRetriever> delimiterRetrievers, IDelimiterTypeDeterminator delimiterTypeDeterminator)
        {
            _delimiterRetrievers = delimiterRetrievers;
            _delimiterTypeDeterminator = delimiterTypeDeterminator;
        }

        public string GetDelimiter(string stringCalculatorInput)
        {   
            DelimiterType typeOfDelimiter = _delimiterTypeDeterminator.Determine(stringCalculatorInput);

            return _delimiterRetrievers[typeOfDelimiter].GetRegexPreparedDelimiter(stringCalculatorInput);
        }

        public bool NoDelimiterIsSpecified(string stringCalculatorInput)
        {
            return _delimiterTypeDeterminator.Determine(stringCalculatorInput).Equals(DelimiterType.Default);
        }

        public IEnumerable<string> GetStringNumbers(string stringCalculatorInput)
        {
            string numbersAndDelimitersFromCalculatorInput = GetStringAfterDelimiterDeclaration(stringCalculatorInput);

            return Regex.Split(numbersAndDelimitersFromCalculatorInput, GetDelimiter(stringCalculatorInput));
        }

        private string GetStringAfterDelimiterDeclaration(string stringCalculatorInput)
        {
            if (NoDelimiterIsSpecified(stringCalculatorInput))
                return stringCalculatorInput;

            int firstCharacterAfterDelimiterDeclaration = stringCalculatorInput.IndexOf(SlashN, StringComparison.Ordinal) + 1;
            int lengthOfNumbersIncludingDelimeters = stringCalculatorInput.Length - firstCharacterAfterDelimiterDeclaration;

            return stringCalculatorInput.Substring(firstCharacterAfterDelimiterDeclaration, lengthOfNumbersIncludingDelimeters);
        }
    }
}