﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StringCalculator.Interfaces;

namespace StringCalculator.DelimiterRetrievers
{
    public class MultiSequenceDelimiterRetriever : IDelimiterRetriever
    {
        private readonly IRegexStringEscaper _regexStringEscaper;

        public MultiSequenceDelimiterRetriever(IRegexStringEscaper regexStringEscaper)
        {
            _regexStringEscaper = regexStringEscaper;           
        }

        public string GetRegexPreparedDelimiter(string stringCalculatorInput)
        {
            int numberOfSequences = GetNumberOfDelimiterSequences(stringCalculatorInput);
            List<string> sequences = GetSequences(stringCalculatorInput, numberOfSequences);

            StringBuilder regexPreparedBuilder = new StringBuilder();
            regexPreparedBuilder.Append("[");
            foreach (var sequence in sequences)
            {
                regexPreparedBuilder.Append(_regexStringEscaper.GetStringEscapedForUseAsRegex(sequence));
            }
            regexPreparedBuilder.Append("]");
            
            return regexPreparedBuilder.ToString();
        }

        private List<string> GetSequences(string stringCalculatorInput, int numberOfSequences)
        {
            List<string> sequences = new List<string>(numberOfSequences);

            var splitByOpenBracket = stringCalculatorInput.Split('[');
            foreach (var sequence in splitByOpenBracket.Skip(1))
            {
                sequences.Add(sequence.Substring(0, sequence.IndexOf(']')));
            }

            return sequences;
            /*               
  * [0]: "//"
    [1]: "Q]"
    [2]: "W]\n1Q2W3"
  */
        }

        private int GetNumberOfDelimiterSequences(string stringCalculatorInput)
        {
            int numberOfOpenBrackets = stringCalculatorInput.GetCountOfOpenBrackets();
            int numberOfClosingBrackets = stringCalculatorInput.GetCountOfOpenBrackets();

            ThrowExceptionIfNumberOfSequencesIsNotGreaterThanOneOrCannotDetermineSequenceDefinitions(numberOfOpenBrackets, numberOfClosingBrackets);

            return numberOfOpenBrackets;
        }

        private void ThrowExceptionIfNumberOfSequencesIsNotGreaterThanOneOrCannotDetermineSequenceDefinitions(int numberOfOpenBrackets, int numberOfClosingBrackets)
        {
            if(numberOfOpenBrackets <=1 || numberOfClosingBrackets <=1)
                throw new ArgumentOutOfRangeException("There aren't enough Open/Closed Brackets ([ or ]) for a MultiSequenceDelimiter. " + GetNumberOfOpeningAndClosingBracketsMessage(numberOfOpenBrackets, numberOfClosingBrackets));

            if(!numberOfOpenBrackets.Equals(numberOfClosingBrackets))
                throw new ArgumentOutOfRangeException("Number of opening and closing brackets is not equal. " + GetNumberOfOpeningAndClosingBracketsMessage(numberOfOpenBrackets, numberOfClosingBrackets));
        }

        private static string GetNumberOfOpeningAndClosingBracketsMessage(int numberOfOpenBrackets, int numberOfClosingBrackets)
        {
            return " numberOfOpenBrackets = " + numberOfOpenBrackets + " numberOfClosingBrackets = " + numberOfClosingBrackets;
        }
    }
}