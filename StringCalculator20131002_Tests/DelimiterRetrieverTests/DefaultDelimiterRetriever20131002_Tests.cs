﻿using NUnit.Framework;
using StringCalculator.DelimiterRetrievers;

namespace DefaultDelimiterRetriever20131002_Tests
{

    [TestFixture]
    public class GetRegexPreparedDelimiter
    {
        [Test]
        public void ReturnsOpenSquareBracketCommaBackslashNClosedBracket_WhenPassedDefaultAsDelimiterType()
        {
            const string expectedDelimiterOfOpenBracketCommaBackslashNClosedBracket = "[,\n]";
            const string stringCalculatorInput = "1\n2,3";

            IDelimiterRetriever defaultDelimiterRetriever = new DefaultDelimiterRetriever();
            var actualDelimiter = defaultDelimiterRetriever.GetRegexPreparedDelimiter(stringCalculatorInput);

            Assert.That(actualDelimiter, Is.EqualTo(expectedDelimiterOfOpenBracketCommaBackslashNClosedBracket));
        }
    }
}
