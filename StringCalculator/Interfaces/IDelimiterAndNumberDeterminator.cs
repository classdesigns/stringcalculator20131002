using System.Collections.Generic;

namespace StringCalculator.Interfaces
{
    public interface IDelimiterAndNumberDeterminator
    {
        string GetDelimiter(string stringCalculatorInput);
        bool NoDelimiterIsSpecified(string stringCalculatorInput);
        IEnumerable<string> GetStringNumbers(string stringCalculatorInput);
    }
}